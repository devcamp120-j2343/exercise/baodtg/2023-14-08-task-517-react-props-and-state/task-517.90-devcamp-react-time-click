import { Component } from "react";

class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            output: []
        }
    }
    onBtnClick = () => {
        let hour = new Date().getHours();
        let minute = new Date().getMinutes();
        let second = new Date().getSeconds();

        let newformat = this.state.hour >= 12 ? 'PM' : 'AM';
        let hours = hour > 12 ? hour % 12 : hour

        let time = `${hours}:${minute}:${second} ${newformat}`

        console.log(time)
        this.setState({
            output: [...this.state.output, time]
        })
    }
    render() {

        return (
            <>
                <p>List:</p>

                {this.state.output.map((element, index) => {
                    return <h3 key={index}>{element} </h3>
                })}

                <button onClick={this.onBtnClick}>Click</button>


            </>

        )
    }
}
export default Content